let UNDEF = 'undefined';
var bc;
if (typeof BroadcastChannel !== UNDEF) {
  bc = new BroadcastChannel('rvi');
}

var UPDATE_SLACK = 1 * 24 * 60 * 60; // Limit nagging to daily
var CACHE = 'PLACEHOLDER_LBL';
var OFFLINE = './';
var CACHED_URLS = [
  './?pwa',
  'favicon.ico',
  'i.png',
  'o.png',
  'w.png',
  OFFLINE
];

self.addEventListener('install', (evt) => {
  evt.waitUntil(
    caches.open(CACHE).then(
      cache => Promise.all(
        CACHED_URLS.map(url => {
          return fetch(`${url}`).then(response => {
            if (!response.ok) {
              throw Error('Not ok '.concat(`${url}`));
            }
            return cache.put(url, response);
          })
        })
      )
    )
  );
  if (bc) {
    bc.postMessage({update_slack: UPDATE_SLACK});
  }
});

self.addEventListener('activate', (evt) => {
  evt.waitUntil(
    caches.keys().then((labels) => {
      return Promise.all(
        labels.map((label) => {
          if (CACHE !== label) {
            return caches.delete(label);
          }
        })
      );
    })
  );
});

self.addEventListener('fetch', (evt) => {
  evt.respondWith(caches.match(evt.request).then((response) => {
    if (response !== undefined) {
      return response;
    } else {
      return fetch(evt.request).then( (response) => {
        if (response.ok) {
          let responseClone = response.clone();
          caches.open(CACHE).then( (cache) => {
            cache.put(evt.request, responseClone);
          });
        }
        return response;
      }).catch( () => {
        return caches.match(OFFLINE);
      });
    }
  }));
});

self.addEventListener('message', (evt) => {
  if (evt.data === 'skip_waiting') {
    self.skipWaiting();
  }
});
