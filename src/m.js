(() => {

  // Limit nagging to daily. This is only the client's default value that is
  // updated from the installing Service Worker when BroadcastChannel is
  // available in the browser:
  var update_slack = 1 * 24 * 60 * 60;
  const CLICK = 'click';
  let UNDEF = 'undefined';
  var timer;
  var bc;
  if (typeof BroadcastChannel !== UNDEF) {
    bc = new BroadcastChannel('rvi');
  }

  rig_yb = function() {
    if (window.localStorage) {
      var KEY = 'rvi-yb';
      var TRUE = 1;
      var chk = document.getElementById(KEY);
      if (localStorage.getItem(KEY) == TRUE) {
        chk.checked = true;
      }
      chk.addEventListener(CLICK, (evt) => {
        var tgt = evt.target;
        localStorage.setItem(KEY, tgt.checked ? 1 : 0);
      });
    }
  }

  function cf_nag(par) {
    var reg = par;
    return function() {
      let btn = document.createElement('button');
      let txt = document.createTextNode("Refresh app now?");
      btn.appendChild(txt);
      btn.classList.add('rvi-nag');
      btn.addEventListener(CLICK, (evt) => {
        if (reg.waiting) {
          reg.waiting.postMessage('skip_waiting');
        }
        let tgt = evt.target;
        tgt.remove();
      });
      document.body.appendChild(btn);
    }
  }

  function go_update(reg) {
    if (timer) {
      window.clearTimeout(timer);
    }
    timer = window.setTimeout(cf_nag(reg), update_slack * 1000);
  }

	rig_sw = function() {
		if ('serviceWorker' in navigator) {
		  navigator.serviceWorker.register('sw.js').then((reg) => {
        if (reg.waiting) {
          go_update(reg);
        }
        reg.addEventListener('updatefound', () => {
          if (reg.installing) {
            reg.installing.addEventListener('statechange', () => {
              if (reg.waiting) {
                if (navigator.serviceWorker.controller) {
                  go_update(reg);
                }
              }
            });
          }
        });
        let refreshing = false;
        navigator.serviceWorker.addEventListener('controllerchange', () => {
          if (!refreshing) {
            window.location.reload();
            refreshing = true;
          }
        });
        if (bc) {
          bc.addEventListener('message', (evt) => {
            if ('update_slack' in evt.data) {
              update_slack = evt.data.update_slack;
            }
          });
        }
		  }).catch(() => {
		  });
		}
	}

  var rigs = [
    rig_yb,
	  rig_sw
  ];

  document.addEventListener('readystatechange', () => {
    if (document.readyState == 'complete') {
      for (var i = 0; i < rigs.length; i++) {
        window.setTimeout(rigs[i], 0);
      }
    }
  });

})()
