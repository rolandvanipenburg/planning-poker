# This should be placed in httpd.conf in a Directory section because for
# performance .htaccess is not the optimal way.
AddCharset UTF-8 html txt css js json webmanifest svg
FileETag None

AddType 'application/manifest+json; charset=utf-8' webmanifest
AddType image/x-icon .ico

DirectoryIndex index.html

ErrorDocument 404 /404.html

RewriteEngine On
RewriteCond %{HTTP_HOST} !^(cdn\.)?rolandvanipenburg\.com$
RewriteRule ^ https://rolandvanipenburg.com%{REQUEST_URI} [L,R=301]

RewriteRule "^sw_(.*)\.js$" "sw.js" [PT]

AddEncoding gzip .gz
AddEncoding br .br

RewriteCond %{HTTP:Accept-Encoding} br
RewriteCond %{REQUEST_FILENAME}.br -f
RewriteRule ^(.*)$ $1.br [L]

RewriteCond %{HTTP:Accept-Encoding} gzip
RewriteCond %{REQUEST_FILENAME}.gz -f
RewriteRule ^(.*)$ $1.gz [L]

# Serve correct content types, and prevent mod_deflate double gzip.
RewriteRule \.html\.gz$ - [T=text/html;charset=utf-8,E=no-gzip:1]
RewriteRule \.html\.br$ - [T=text/html;charset=utf-8,E=no-gzip:1]
RewriteRule \.css\.gz$ - [T=text/css;charset=utf-8,E=no-gzip:1]
RewriteRule \.css\.br$ - [T=text/css;charset=utf-8,E=no-gzip:1]
RewriteRule \.js\.gz$ - [T=text/javascript;charset=utf-8,E=no-gzip:1]
RewriteRule \.js\.br$ - [T=text/javascript;charset=utf-8,E=no-gzip:1]
RewriteRule \.json\.gz$ - [T=application/json;charset=utf-8,E=no-gzip:1]
RewriteRule \.json\.br$ - [T=application/json;charset=utf-8,E=no-gzip:1]
RewriteRule \.webmanifest\.gz$ - [T=application/manifest+json;charset=utf-8,E=no-gzip:1]
RewriteRule \.webmanifest\.br$ - [T=application/manifest+json;charset=utf-8,E=no-gzip:1]
RewriteRule \.ico\.gz$ - [T=image/x-icon,E=no-gzip:1]
RewriteRule \.ico\.br$ - [T=image/x-icon,E=no-gzip:1]
RewriteRule \.svg\.gz$ - [T=image/svg+xml;charset=utf-8,E=no-gzip:1]
RewriteRule \.svg\.br$ - [T=image/svg+xml;charset=utf-8,E=no-gzip:1]
RewriteRule \.eot\.gz$ - [T=application/vnd.ms-fontobject,E=no-gzip:1]
RewriteRule \.eot\.br$ - [T=application/vnd.ms-fontobject,E=no-gzip:1]
RewriteRule \.ttf\.gz$ - [T=application/font-sfnt,E=no-gzip:1]
RewriteRule \.ttf\.br$ - [T=application/font-sfnt,E=no-gzip:1]

<IfModule mod_headers.c>
  Header always set X-Frame-Options "SAMEORIGIN"
  Header always set Content-Security-Policy "default-src 'self' *.rolandvanipenburg.com"
  # Set files matching a hashed pattern to immutable:
  <FilesMatch ".+_[[:xdigit:]]{7}\.(css|js|json|webmanifest|svg|eot|ttf|woff|woff2|ico|jpg|png|webp)(\.(gz|br))?$">
    Header merge Cache-Control "max-age=31536000"
    Header merge Cache-Control "immutable"
  </FilesMatch>
	SetEnvIf Request_URI "(.*?)(\.(br|gz))?$" Request_Uri=$1
	Header set Link '<https://rolandvanipenburg.com%{Request_Uri}e>; rel="canonical"'
	Header set X-Nl-Kvk '68824777'
	Header always set Strict-Transport-Security "max-age=31536001; includeSubDomains; preload"
	Header always set X-Content-Type-Options "nosniff"
	<FilesMatch "\.(html|css|js|json|webmanifest|svg|eot|ttf|ico|gz|br)$">
		Header append Vary: Accept-Encoding
	</FilesMatch>
	<FilesMatch "\.(html)(\.(gz|br))?$">
		Header merge Cache-Control "no-cache"
    Header merge Cache-Control "no-transform"
	</FilesMatch>
  <FilesMatch "^sw(_.*)?\.js$">
    Header set Cache-Control "max-age=0, no-cache"
  </FilesMatch>
  Header set Access-Control-Allow-Origin https://rolandvanipenburg.com
</IfModule>
