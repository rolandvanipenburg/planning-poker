#!/bin/bash
# Create a zip for deployment on Hostnet Cloud
JELASTIC=planning_poker_deck
CDN=https://rolandvanipenburg-mirh.cdn.edgeport.net
CDN=""
DATE=$(date +"%Y%m%d%H%M")
SEPARATOR=_
UNDER=_
SLASH="/"
DOT="."
TMPSFX=.tmp
ori=`pwd`
git=`git rev-parse --show-toplevel`
rm -Rf $git/build
mkdir $git/build
cp -Rp $git/src/ $git/build
cd $git/build
rm _htaccess
/usr/bin/find -E . -iregex '.*\.(jpg)$' \
	-exec echo JPEG-optimizing '{}' + \
	-exec jpegtran -copy none -optimize -outfile '{}'$TMPSFX '{}' \; \
	-exec mv '{}'$TMPSFX '{}' \;
/usr/bin/find -E . -iregex '.*\.(css)$' \
	-exec echo CleanCSS-compressing '{}' + \
	-exec cleancss -o '{}'$TMPSFX '{}' \; \
	-exec mv '{}'$TMPSFX '{}' \;
/usr/bin/find -E . -iregex '.*\.(js)$' \
	-exec echo UglifyJS-compressing '{}' + \
	-exec uglifyjs '{}' -c -m -o '{}'$TMPSFX \; \
	-exec sh -c 'if ! grep -q "define(function(require, exports, module)" {} ; then mv '{}'.tmp '{}'; else rm '{}'.tmp; fi' \;
/usr/bin/find -E . -iregex '.*\.(json|webmanifest)$' \
	-exec echo jq compressing '{}' \; \
	-exec sh -c "cat {} | jq -c . > {}$TMPSFX" \; \
	-exec mv '{}'$TMPSFX '{}' \;
/usr/bin/find -E . -iregex '.*\.(html)$' \
	-exec echo Hyphenating '{}' + \
	-exec sh -c "hyphenate_html {} > {}$TMPSFX" \; \
	-exec mv '{}'$TMPSFX '{}' \;
if [[ -n $CDN ]]; then
	/usr/bin/find -E . -iregex '.*\.(html)$' \
		-exec echo Injecting preconnect '{}' + \
		-exec /usr/bin/sed -i .bak "s|<head>|<head><link rel=preconnect href=$CDN>|g" {} \;
	CDN+=$SLASH
fi
/usr/bin/find -E . -iregex '.*\.(html)$' \
	-exec echo Inlining '{}' + \
	-exec sh -c "cat {} | ../bin/inline.pl > {}$TMPSFX" \; \
	-exec mv '{}'$TMPSFX '{}' \;
/usr/bin/find -E . -iregex '.*\.(html)$' \
	-exec echo Minifying '{}' + \
	-exec html-minifier -c $git/bin/html-minifier.conf '{}' -o '{}'$TMPSFX \; \
	-exec mv '{}'$TMPSFX '{}' \;
/usr/bin/find -E . -iregex '.*\.(js|json|css|webmanifest)$'\
	-exec echo Removing EOF in '{}' + \
	-exec perl -pi -e 'chomp if eof' '{}' \;
# First replace the longer extensions so they aren't matched by the shorters:
exts=( woff2 woff json js svg eot ttf jpg png webp ico webmanifest css )
for ext in "${exts[@]}"
do
/usr/bin/find -E . -iregex '.*\.('$ext')$' ! -iname 'sw.js'\
	-exec echo Cache busting '{}' + \
	-exec sh -c 'old="$(echo ${0#./})"; new="${0%.*}$1$(md5 $0|cut -d\  -f4|head -c7)$2${0##*.}"; new=${new#./}; cp $old $new; /usr/bin/sed -i .bak "s|$old|$3$new|g" *.js *.css *.webmanifest *.html' {} $UNDER $DOT $CDN \;
done
/usr/bin/find . -iname 'sw.js'\
	-exec echo Update cache label in '{}' + \
	-exec sh -c 'lbl="$(cat {} *.html|md5|head -c7)"; /usr/bin/sed -i .bak "s|PLACEHOLDER_LBL|$lbl|" sw.js' \;
/usr/bin/find . -iname 'index.html'\
	-exec echo Update version label in '{}' + \
	-exec sh -c 'lbl="$(cat sw.js|md5|head -c7)"; /usr/bin/sed -i .bak "s|PLACEHOLDER_LBL|$lbl|" index.html' \;
/usr/bin/find -E . -iregex '.*\.(js|json|css|webmanifest)$'\
	-exec echo Removing EOF in '{}' + \
	-exec perl -pi -e 'chomp if eof' '{}' \;
/usr/bin/find -E . -iregex '.*\.(bak)$' \
	-exec echo Removing backups '{}' + \
	-exec rm {} \;
/usr/bin/find -E . -iregex '.*\.(html|js|json|webmanifest|css|svg|eot|ttf|woff|ico)$' \
	-exec echo Compressing '{}' + \
	-exec zopfli -k '{}' + \
	-exec brotli --quality=11 --force --output='{}'.br '{}' \;
zip -r $JELASTIC$SEPARATOR$DATE *
cd $ori
