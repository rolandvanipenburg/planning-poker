#!/usr/bin/env perl -w    # -*- cperl; cperl-indent-level: 4 -*-
# Copyright (C) 2018, Roland van Ipenburg
use strict;
use warnings;

use utf8;
use 5.014000;

BEGIN { our $VERSION = '0.002'; }

use HTML::Parser;
use Path::Tiny qw/path/;

sub start {
	my ($tagname, $hr, $text) = @_;
	if ($tagname eq 'link') {
		if (exists $hr->{'rel'} && $hr->{'rel'} eq 'stylesheet') {
			if (exists $hr->{'href'}) {
				print '<style><!--';
				my $css = path($hr->{'href'});
				print $css->slurp_utf8;
				print '--></style>';
				return;
			}
		}
	}
	if ($tagname eq 'script') {
		if (exists $hr->{'src'}) {
            print '<script>';
            my $js = path($hr->{'src'});
            print $js->slurp_utf8;
            print '</script>';
            return;
        }
    }
	print $text;
}

my $p = HTML::Parser->new(
	api_version => 3,
	default_h	=> [sub { print shift }, 'text'],
    start_h 	=> [\&start, 'tagname, attr, text'],
    marked_sections => 1,
);
while(<>) {
	$p->parse($_);
}
