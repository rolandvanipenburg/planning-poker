module.exports = {
  ci: {
	collect: {
      staticDistDir: './build',
    },
    assert: {
      preset: 'lighthouse:all',
      assertions: {
        'long-tasks': { 'minScore': 0 }
      }
    },
    upload: {
      target: 'lhci',
      serverBaseUrl: 'https://lh.rolandvanipenburg.com',
      token: 'a8e7b828-128e-4ce3-9234-173e8f6e4a7e',
    },
  },
};
